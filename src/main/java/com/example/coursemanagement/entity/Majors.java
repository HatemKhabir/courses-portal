package com.example.coursemanagement.entity;

public enum Majors {
    COMPUTER_SCIENCE,
    MATHEMATICS,
    PHYSICS,
    CHEMISTRY,
    BIOLOGY,
    ENGINEERING,
    BUSINESS,
    LITERATURE
}
