package com.example.coursemanagement.entity;

public enum Languages {
    ENGLISH,
    FRENCH,
    ARABIC,
    HUNGARIAN,
    GERMAN,
    SPANISH,
    TURKISH
}
