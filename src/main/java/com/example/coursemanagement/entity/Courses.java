package com.example.coursemanagement.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.*;

import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "courses")
@NoArgsConstructor
@Data
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Courses extends AbstractEntity {

    @Column(name = "capacity", nullable = false)
    @NotNull(message = "please enter a capacity!")
    @Positive(message = "capacity must be positive!")
    private int capacity;

    @Enumerated(EnumType.STRING)
    @Column(name = "language", nullable = false)
    @NotEmpty(message = "enter a Language")
    private Languages language;

    @Column(name = "room_number", nullable = false)
    @NotNull(message = "please enter a number!")
    @Positive(message = "room number must be positive!")
    private int roomNum;

    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @Getter
    @Setter
    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.ALL })
    @JoinTable(
            name = "student_course",
            joinColumns = @JoinColumn(name="course_id"),
            inverseJoinColumns = @JoinColumn(name = "student_id")
    )
    private Set<Student> students = new HashSet<>();

    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @Getter
    @Setter
    @OneToOne
    @JoinColumn(name = "teacher_id")
    private Teacher teacher;

    @Column(name = "credits", nullable = false)
    @NotNull(message = "please enter credits!")
    @Positive(message = "credits must be positive!")
    private int credits;
}
