package com.example.coursemanagement.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;
import lombok.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.validation.constraints.*;

@Entity
@Table(name = "students")
@NoArgsConstructor
@AllArgsConstructor
@Data
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Student extends AbstractEntity {

    @Column(name = "age", nullable = false)
    @NotNull(message = "Enter an age!")
    @Min(value = 0, message = "Age must be positive!")
    private int age;

    @Enumerated(EnumType.STRING)
    @Column(name = "major", nullable = false)
    @NotNull(message = "Select a major")
    private Majors major;

    @Setter
    @Getter
    @ManyToMany(fetch = FetchType.LAZY,cascade = { CascadeType.ALL })
    @JoinTable(
            name = "student_course",
            joinColumns = @JoinColumn(name = "student_id"),
            inverseJoinColumns = @JoinColumn(name = "course_id")
    )
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    private List<Courses> courses ;

    @ManyToOne
    @JoinColumn(name = "teacher_id")
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    private Teacher teachers;
    public void addCourse(Courses course) {
        courses.add(course);
        course.getStudents().add(this);
    }

    public void removeCourse(Courses course) {
        courses.remove(course);
        course.getStudents().remove(this);
    }
}
