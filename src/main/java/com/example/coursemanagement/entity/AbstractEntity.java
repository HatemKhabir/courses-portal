package com.example.coursemanagement.entity;

import jakarta.persistence.GenerationType;
import jakarta.persistence.*;
import jakarta.persistence.MappedSuperclass;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;

@NoArgsConstructor
@Data
@SuperBuilder
@MappedSuperclass
public class AbstractEntity {

    @Id
    @Column(name = "id", unique = true, updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY  )
    private Long id;

    @Column(name = "name", nullable = false, unique = true)
    @Length(min = 2, max = 50)
    @NotEmpty(message = " name required")
    private String name;


}
