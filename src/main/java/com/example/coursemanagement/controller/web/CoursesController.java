package com.example.coursemanagement.controller.web;

import com.example.coursemanagement.entity.Courses;
import com.example.coursemanagement.entity.Languages;
import com.example.coursemanagement.service.CoursesService;
import jakarta.el.PropertyNotFoundException;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/courses")
@Log4j2
public class CoursesController {
    private final CoursesService coursesService;

    public CoursesController(CoursesService coursesService){
        this.coursesService = coursesService;
    }

    @GetMapping
    public String getAllCourses(Model model, @ModelAttribute("successMessage") String successMessage) {
        log.info("Fetching all courses");
        model.addAttribute("courses", coursesService.getAll());
        if (successMessage != null && !successMessage.isEmpty()) {
            model.addAttribute("successMessage", successMessage);
            log.info("Displaying success message: {}", successMessage);
        }
        return "courses/courses-list";
    }


    @GetMapping("/{id}")
    public String getCourseById(@PathVariable Long id, Model model) {
        log.info("Fetching course with ID: {}", id);
        Courses course = coursesService.getCourseById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid Course ID: " + id));
        model.addAttribute("course", course);
        log.info("Returning course details for course ID: {}", id);
        return "courses/courses-id";
    }


    @GetMapping("/add")
    public String showAddCourseForm(Model model) {
        model.addAttribute("course", new Courses());
        model.addAttribute("languages", Languages.values());
        return "courses/courses-form";
    }

    @PostMapping("/add")
    public String addCourse(@Valid @ModelAttribute("course") Courses course, BindingResult result, RedirectAttributes redirectAttributes){
        if (result.hasErrors()) {
            log.error("Validation errors occurred while adding new course: {}", result.getAllErrors());
            return "courses/courses-form";
        }
        coursesService.saveOrUpdateCourse(course);
        log.info("New course created: {}", course.getName());
        redirectAttributes.addFlashAttribute("successMessage", "Course created successfully");
        return "redirect:/courses";
    }

    @GetMapping("/edit/{id}")
    public String showEditCourseForm(@PathVariable Long id, Model model) {
        Courses course = coursesService.getCourseById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid Course ID: " + id));
        model.addAttribute("course", course);
        model.addAttribute("languages", Languages.values());

        return "courses/courses-form";
    }

    @PutMapping("/edit/{id}")
    public String updateCourse(@PathVariable Long id, @Valid @ModelAttribute("course") Courses course, BindingResult result, RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            return "courses/courses-form";
        }
        course.setId(id); // Ensure the course ID is set for update
        coursesService.saveOrUpdateCourse(course);
        redirectAttributes.addFlashAttribute("successMessage", "Course updated successfully");
        return "redirect:/courses";
    }
    @DeleteMapping("/delete/{id}")
    public String deleteCourse(@PathVariable Long id, RedirectAttributes redirectAttributes) {
        log.info("Attempting to delete course with ID: {}", id);
        try {
            coursesService.deleteCourse(id);
            redirectAttributes.addFlashAttribute("successMessage", "Course deleted successfully");
            log.info("Course deleted successfully with ID: {}", id);
        } catch (Exception e) {
            log.error("Failed to delete course with ID: {}. Error: {}", id, e.getMessage(), e);
            redirectAttributes.addFlashAttribute("errorMessage", "Failed to delete course: " + e.getMessage());
        }
        return "redirect:/courses";
    }
}
