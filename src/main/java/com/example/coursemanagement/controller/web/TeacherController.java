package com.example.coursemanagement.controller.web;

import com.example.coursemanagement.entity.Courses;
import com.example.coursemanagement.entity.Teacher;
import com.example.coursemanagement.service.CoursesService;
import com.example.coursemanagement.service.TeacherService;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Optional;

@Controller
@RequestMapping("/teachers")
@Log4j2
public class TeacherController {

    private final TeacherService teacherService;
    private final CoursesService coursesService;
    public TeacherController(TeacherService teacherService,CoursesService coursesService) {
        this.teacherService = teacherService;
        this.coursesService=coursesService;
    }

    @GetMapping
    public String getAllTeachers(Model model) {
        model.addAttribute("teachers", teacherService.getAllTeachers());
        return "teachers/teacher-list";
    }

    @GetMapping("/{id}")
    public String getTeacherById(@PathVariable Long id, Model model) {
        Optional<Teacher> teacher = teacherService.getTeacherById(id);
        if (teacher.isEmpty()) {
            throw new IllegalArgumentException("Invalid Teacher ID: " + id);
        }
        model.addAttribute("teacher", teacher.get());
        return "teachers/teacher-id";
    }


    @GetMapping("/by-course")
    public String getTeacherByCourse(@RequestParam Long courseId, Model model) {
        Courses course = coursesService.getCourseById(courseId)
                .orElseThrow(() -> new IllegalArgumentException("Invalid Course ID: " + courseId));
        Teacher teacher=course.getTeacher();
        model.addAttribute("teacher", teacher);
        return "teachers/teacher-course";
    }

    @GetMapping("/add")
    public String showAddTeacherForm(Model model) {
        model.addAttribute("teacher", new Teacher());
        model.addAttribute("courses",coursesService.getAll());
        return "teachers/teacher-form";
    }

    @PostMapping("/add")
    public String addTeacher(@ModelAttribute("teacher") Teacher teacher,
                             @RequestParam(required = false) Long courseId,
                             BindingResult result,
                             RedirectAttributes redirectAttributes,
                             Model model) {
        log.info("Adding new teacher");
        if (result.hasErrors()) {
            log.error("Validation errors while adding new teacher: {}", result.getAllErrors());
            model.addAttribute("errorMessage", "Please correct the form errors.");
            return "teachers/teacher-form";
        }
        try {
            teacherService.saveOrUpdateTeacher(null, teacher, courseId);
            redirectAttributes.addFlashAttribute("successMessage", "Teacher created successfully");
            return "redirect:/teachers";
        } catch (RuntimeException ex) {
            log.error("Exception while adding teacher: {}", ex.getMessage(), ex);
            model.addAttribute("errorMessage", ex.getMessage());
            model.addAttribute("courses", coursesService.getAll());
            return "teachers/teacher-form";
        }
    }
    @GetMapping("/edit/{id}")
    public String showEditTeacherForm(@PathVariable Long id, Model model) {
        Optional<Teacher> teacher = teacherService.getTeacherById(id);
        if (teacher.isPresent()) {
            model.addAttribute("teacher", teacher.get());
            model.addAttribute("courses",coursesService.getAll());
            return "teachers/teacher-form";
        } else {
            throw new IllegalArgumentException("Invalid Teacher ID: " + id);
        }
    }



    @PutMapping("/edit/{id}")
    public String updateTeacher(@PathVariable Long id, @RequestParam("courseId") Long courseId, @ModelAttribute("teacher") Teacher teacher, BindingResult result, Model model, RedirectAttributes redirectAttributes) {
        log.info("Updating teacher with ID: {}", id);
        if (result.hasErrors()) {
            log.error("Validation errors while updating teacher ID {}: {}", id, result.getAllErrors());
            model.addAttribute("courses", coursesService.getAll());  // Ensure courses are repopulated for the form dropdown
            return "teachers/teacher-form";
        }
        try {
            teacher.setId(id);  // Set ID to ensure updates are applied to the correct entity
            teacherService.saveOrUpdateTeacher(id, teacher, courseId);
            redirectAttributes.addFlashAttribute("successMessage", "Teacher updated successfully");
            return "redirect:/teachers";
        } catch (Exception ex) {
            log.error("Error updating teacher with ID {}: {}", id, ex.getMessage(), ex);
            model.addAttribute("errorMessage", "Error Editing Teacher: " + ex.getMessage());
            model.addAttribute("courses", coursesService.getAll());  // Repopulate courses for form dropdown
            return "teachers/teacher-form";  // Stay on the same page to display error
        }
    }


    @DeleteMapping("/delete/{id}")
    public String deleteTeacher(@PathVariable Long id, RedirectAttributes redirectAttributes) {
        log.info("Attempting to delete teacher with ID: {}", id);
        try {
            teacherService.deleteTeacher(id);
            redirectAttributes.addFlashAttribute("successMessage", "Teacher deleted successfully");
            log.info("Successfully deleted teacher with ID: {}", id);
        } catch (Exception e) {
            log.error("Error deleting teacher with ID {}: {}", id, e.getMessage(), e);
            redirectAttributes.addFlashAttribute("errorMessage", "Error deleting teacher: " + e.getMessage());
        }
        return "redirect:/teachers";
    }
}
