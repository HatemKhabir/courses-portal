package com.example.coursemanagement.controller.web;

import com.example.coursemanagement.entity.Courses;
import com.example.coursemanagement.entity.Majors;
import com.example.coursemanagement.entity.Student;
import com.example.coursemanagement.service.CoursesService;
import com.example.coursemanagement.service.StudentService;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.*;

@Controller
@RequestMapping("/students")
@Log4j2
public class StudentController {
    private final StudentService studentService;
    private final CoursesService coursesService;

    public StudentController(StudentService studentService, CoursesService coursesService) {
        this.studentService = studentService;
        this.coursesService = coursesService;
    }

    @GetMapping
    public String getAllStudents(Model model, @ModelAttribute("successMessage") String successMessage) {
        model.addAttribute("students", studentService.getAll());
        if (successMessage != null && !successMessage.isEmpty()) {
            model.addAttribute("successMessage", successMessage);
        }
        return "students/students-list";
    }

    @GetMapping("/{id}")
    public String getStudentById(@PathVariable Long id, Model model) {
        log.debug("Getting student by ID: {}", id);
        Student student = studentService.getStudentById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid student ID: " + id));
        model.addAttribute("student", student);
        model.addAttribute("courses", studentService.getCourses(id));
        return "students/student-id";
    }

    @GetMapping("/by-major/{major}")
    public String getStudentByMajor(@PathVariable Majors major, Model model) {
        log.debug("Getting student by major: {}", major);

        List<Student> students = studentService.getStudentByMajor(major);
        if (students.isEmpty()) {
            return "students/major-error";
        }
        model.addAttribute("students", students);
        return "students/students-major";
    }

    @GetMapping("/add")
    public String showAddStudentForm(Model model) {
        model.addAttribute("student", new Student());
        model.addAttribute("courses", coursesService.getAll());
        model.addAttribute("majors", Majors.values());
        return "students/student-form";
    }

    @PostMapping("/add")
    public String addStudent(@Valid @ModelAttribute("student") Student student, @RequestParam(required = false) List<Long> selectedCourseIds, BindingResult result, Model model, RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            model.addAttribute("courses", coursesService.getAll());
            model.addAttribute("majors", Majors.values());
            return "students/student-form";
        }

        List<Courses> selectedCourses = coursesService.findAllByIds(selectedCourseIds);
        student.setCourses(selectedCourses);
        studentService.saveOrUpdateStudent(student);
        redirectAttributes.addFlashAttribute("successMessage", "Student created successfully");
        return "redirect:/students";
    }

    @GetMapping("/edit/{id}")
    public String showEditStudentForm(@PathVariable Long id, Model model) {
        Student student = studentService.getStudentById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid student ID: " + id));
        List<Courses> courses = coursesService.getAll();

        // Create a map to indicate if the student is enrolled in the course
        Map<Long, Boolean> courseCheckedMap = new HashMap<>();
        for (Courses course : courses) {
            courseCheckedMap.put(course.getId(), student.getCourses().contains(course));
        }

        model.addAttribute("student", student);
        model.addAttribute("courses", courses);
        model.addAttribute("majors", Majors.values());
        model.addAttribute("courseCheckedMap", courseCheckedMap);
        return "students/student-form";
    }

    @PutMapping("/edit/{id}")
    public String updateStudent(@PathVariable Long id,@RequestParam(required = false) List<Long> selectedCourseIds, @Valid @ModelAttribute("student") Student student, BindingResult result, Model model, RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            log.debug("results of request : "+result);
            List<Courses> courses = coursesService.getAll();
            Map<Long, Boolean> courseCheckedMap = new HashMap<>();
            for (Courses course : courses) {
                courseCheckedMap.put(course.getId(), student.getCourses().contains(course));
            }
            model.addAttribute("courses", courses);
            model.addAttribute("majors", Majors.values());
            model.addAttribute("courseCheckedMap", courseCheckedMap);
            return "students/student-form";
        }
        student.setId(id);
        if (selectedCourseIds!=null && !selectedCourseIds.isEmpty()) {
        List<Courses> selectedCourses=coursesService.findAllByIds(selectedCourseIds);
        student.setCourses(selectedCourses);
        }else{
            student.setCourses(new ArrayList<Courses>());
        }
        // Ensure the student ID is set for update
        studentService.saveOrUpdateStudent(student);
        redirectAttributes.addFlashAttribute("successMessage", "Student updated successfully");
        return "redirect:/students";
    }

    @DeleteMapping("/delete/{id}")
    public String deleteStudent(@PathVariable Long id, RedirectAttributes redirectAttributes) {
        try {
            studentService.deleteStudent(id);
            redirectAttributes.addFlashAttribute("successMessage", "Student deleted successfully");
        } catch (Exception e) {
            log.error("Error deleting student", e);
            redirectAttributes.addFlashAttribute("errorMessage", "Error deleting student: " + e.getMessage());
        }
        return "redirect:/students";
    }
}