package com.example.coursemanagement.controller.rest;

import com.example.coursemanagement.entity.Courses;
import com.example.coursemanagement.entity.Majors;
import com.example.coursemanagement.entity.Student;
import com.example.coursemanagement.service.CoursesService;
import com.example.coursemanagement.service.StudentService;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/students")
@Log4j2
public class StudentRestController {
    public record StudentDTO(String name, int age, Majors major, List<Courses> courses) {}

    private final StudentService studentService;
    private final CoursesService coursesService;

    public StudentRestController(StudentService studentService, CoursesService coursesService) {
        this.studentService = studentService;
        this.coursesService = coursesService;
    }

    @GetMapping
    public ResponseEntity<List<Student>> getAllStudents() {
        log.info("Fetching all students");
        return ResponseEntity.ok(studentService.getAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Student> getStudentById(@PathVariable Long id) {
        log.debug("Fetching student by ID: {}", id);
        Student student = studentService.getStudentById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid student ID: " + id));
        return ResponseEntity.ok(student);
    }

    @GetMapping("/by-major/{major}")
    public ResponseEntity<List<Student>> getStudentByMajor(@PathVariable Majors major) {
        log.debug("Getting students by major: {}", major);
        List<Student> students = studentService.getStudentByMajor(major);
        return students.isEmpty() ? ResponseEntity.noContent().build() : ResponseEntity.ok(students);
    }

    @PostMapping(value = "/add",consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> addStudent( @RequestBody StudentDTO studentDTO) {
        try {
            Student student = new Student();
            student.setName(studentDTO.name());
            student.setAge(studentDTO.age());
            student.setMajor(studentDTO.major());
            student.setCourses(studentDTO.courses);

            studentService.saveOrUpdateStudent(student);
            log.info("Student created successfully: {}", student.getId());
            return ResponseEntity.ok(student);
        } catch (Exception e) {
            log.error("Failed to add student: {}", e.getMessage(), e);
            return ResponseEntity.badRequest().body("Failed to add student: " + e.getMessage());
        }
    }

    @PutMapping(value = "/edit/{id}",consumes = "application/json;charset=UTF-8")
    public ResponseEntity<?> updateStudent(@PathVariable Long id, @Valid @RequestBody Student student) {
        try {
            student.setId(id); // Ensures we update the correct student
            studentService.saveOrUpdateStudent(student);
            log.info("Student updated successfully: {}", student.getId());
            return ResponseEntity.ok(student);
        } catch (Exception e) {
            log.error("Failed to update student ID {}: {}", id, e.getMessage(), e);
            return ResponseEntity.badRequest().body("Failed to update student: " + e.getMessage());
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteStudent(@PathVariable Long id) {
        try {
            studentService.deleteStudent(id);
            log.info("Student deleted successfully: {}", id);
            return ResponseEntity.ok("student deleted");
        } catch (Exception e) {
            log.error("Error deleting student ID {}: {}", id, e.getMessage(), e);
            return ResponseEntity.status(500).body("Error deleting student: " + e.getMessage());
        }
    }
}
