package com.example.coursemanagement.controller.rest;

import com.example.coursemanagement.entity.Courses;
import com.example.coursemanagement.service.CoursesService;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/courses") // It's common to prefix REST API endpoints with /api
@Log4j2
public class CoursesRestController {
    private final CoursesService coursesService;

    public CoursesRestController(CoursesService coursesService) {
        this.coursesService = coursesService;
    }

    @GetMapping
    public ResponseEntity<List<Courses>> getAllCourses() {
        log.info("Fetching all courses");
        List<Courses> courses = coursesService.getAll();
        return ResponseEntity.ok(courses);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Courses> getCourseById(@PathVariable Long id) {
        log.info("Fetching course with ID: {}", id);
        Courses course = coursesService.getCourseById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Invalid Course ID: " + id));
        return ResponseEntity.ok(course);
    }

    @PostMapping
    public ResponseEntity<Void> addCourse(@Valid @RequestBody Courses course) {
        log.info("Adding new course: {}", course.getName());
        try {
            coursesService.saveOrUpdateCourse(course);
            return ResponseEntity.status(HttpStatus.CREATED).build();
        } catch (Exception e) {
            log.error("Failed to add course: {}", e.getMessage(), e);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Error adding the course", e);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateCourse(@PathVariable Long id, @Valid @RequestBody Courses course) {
        log.info("Updating course with ID: {}", id);
        try {
            course.setId(id); // Ensure the course ID is set for update
            coursesService.saveOrUpdateCourse(course);
            return ResponseEntity.ok(course);
        } catch (Exception e) {
            log.error("Failed to update course with ID: {}. Error: {}", id, e.getMessage(), e);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Error updating the course", e);
        }
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteCourse(@PathVariable Long id) {
        log.info("Attempting to delete course with ID: {}", id);
        coursesService.deleteCourse(id);
        return ResponseEntity.ok("Course Deleted ");
    }
}
