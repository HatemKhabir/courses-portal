package com.example.coursemanagement.controller.rest;

import com.example.coursemanagement.entity.Majors;
import com.example.coursemanagement.entity.Teacher;
import com.example.coursemanagement.entity.Courses;
import com.example.coursemanagement.service.TeacherService;
import com.example.coursemanagement.service.CoursesService;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/teachers")
@Log4j2
public class TeacherRestController {
    private final TeacherService teacherService;
    private final CoursesService coursesService;

    public TeacherRestController(TeacherService teacherService, CoursesService coursesService) {
        this.teacherService = teacherService;
        this.coursesService = coursesService;
    }

    @GetMapping
    public ResponseEntity<List<Teacher>> getAllTeachers() {
        List<Teacher> teachers = teacherService.getAllTeachers();
        return ResponseEntity.ok(teachers);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Teacher> getTeacherById(@PathVariable Long id) {
        Optional<Teacher> teacher = teacherService.getTeacherById(id);
        return teacher.map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping("/by-course/{courseId}")
    public ResponseEntity<Teacher> getTeacherByCourse(@PathVariable Long courseId) {
        Courses course = coursesService.getCourseById(courseId)
                .orElseThrow(() -> new IllegalArgumentException("Invalid Course ID: " + courseId));
        return Optional.ofNullable(course.getTeacher())
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping("/add")
    public ResponseEntity<Teacher> addTeacher(@RequestBody Teacher teacher, @RequestParam(required = false) Long courseId) {
        try {
            teacherService.saveOrUpdateTeacher(null, teacher, courseId);
            return ResponseEntity.ok(teacher);
        } catch (Exception e) {
            log.error("Failed to add teacher: {}", e.getMessage());
            return ResponseEntity.badRequest().body(null);
        }
    }

    @PutMapping("/edit/{id}")
    public ResponseEntity<Teacher> updateTeacher(@PathVariable Long id, @Valid @RequestBody Teacher teacher, @RequestParam(required = false) Long courseId) {
        try {
            teacher.setId(id);
            teacherService.saveOrUpdateTeacher(id, teacher, courseId);
            return ResponseEntity.ok(teacher);
        } catch (Exception e) {
            log.error("Error updating teacher: {}", e.getMessage());
            return ResponseEntity.badRequest().body(null);
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteTeacher(@PathVariable Long id) {
        try {
            teacherService.deleteTeacher(id);
            return ResponseEntity.ok("Teacher deleted");
        } catch (Exception e) {
            log.error("Error deleting teacher with ID {}: {}", id, e.getMessage());
            return ResponseEntity.status(500).build();
        }
    }
}