package com.example.coursemanagement.service;

import com.example.coursemanagement.DAO.CoursesRepository;
import com.example.coursemanagement.DAO.StudentRepository;
import com.example.coursemanagement.entity.Courses;
import com.example.coursemanagement.entity.Majors;
import com.example.coursemanagement.entity.Student;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

import java.io.Console;
import java.util.*;

@Service
public class StudentService {
    private final StudentRepository studentRepository;
    private final CoursesRepository coursesRepository;
    public StudentService(StudentRepository studentRepository,CoursesRepository coursesRepository) {
        this.studentRepository = studentRepository;
        this.coursesRepository=coursesRepository;
    }
    public List<Student> getAll(){
        return studentRepository.findAll();
    }
    public Optional<Student> getStudentById(Long id){
        return studentRepository.findById(id);
    }
    public List<Student>  getStudentByMajor(Majors major){
        return studentRepository.findByMajor(major);
    }
    public void saveOrUpdateStudent(Student student){
        studentRepository.save(student);
    }
    @Transactional
    public void deleteStudent(Long studentId) {
        Student student = studentRepository.findById(studentId)
                .orElseThrow(() -> new RuntimeException("Student not found"));
        new ArrayList<>(student.getCourses()).forEach(student::removeCourse);
        studentRepository.delete(student);
    }

    public Optional<List<Courses>> getCourses(Long studentId) {
        Optional<Student> student = studentRepository.findById(studentId);
        return student.map(Student::getCourses);
    }

    public List<Student> findStudentsByCourse(Courses course) {
        return studentRepository.findByCourses(course);
    }
}
