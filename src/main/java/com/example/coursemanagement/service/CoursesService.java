package com.example.coursemanagement.service;

import com.example.coursemanagement.DAO.CoursesRepository;
import com.example.coursemanagement.entity.Courses;
import com.example.coursemanagement.entity.Languages;
import com.example.coursemanagement.entity.Teacher;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CoursesService {
private final CoursesRepository coursesRepository;

    public CoursesService(CoursesRepository coursesRepository) {
        this.coursesRepository = coursesRepository;
    }

    public List<Courses> getAll(){
        return coursesRepository.findAll();
    }

    public Optional<Courses> getCourseById(Long id){
        return coursesRepository.findById(id);
    }

    public List<Courses> findAllByIds(List<Long> ids){return coursesRepository.findAllById(ids);}
    public List<Courses> getCourseByLanguage(Languages lang){
        return coursesRepository.findByLanguage(lang);
    }
    public void saveOrUpdateCourse(Courses course){
        coursesRepository.save(course);
    }
    @Transactional
    public void deleteCourse(Long courseId) {
        Courses course = coursesRepository.findById(courseId)
                .orElseThrow(() -> new RuntimeException("Course not found"));

        new ArrayList<>(course.getStudents()).forEach(student -> {
            student.removeCourse(course);
        });

        coursesRepository.delete(course);
    }
    public Courses findCourseByTeacher(Teacher teacher){
        return coursesRepository.findByTeacher(teacher);
    }

}
