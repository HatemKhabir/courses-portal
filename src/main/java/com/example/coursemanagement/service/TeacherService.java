package com.example.coursemanagement.service;


import com.example.coursemanagement.DAO.CoursesRepository;
import com.example.coursemanagement.DAO.TeacherRepository;
import com.example.coursemanagement.entity.Courses;
import com.example.coursemanagement.entity.Teacher;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TeacherService {
private final TeacherRepository teacherRepository;
private final CoursesRepository coursesRepository;
    public TeacherService(TeacherRepository teacherRepository,CoursesRepository coursesRepository) {
        this.teacherRepository = teacherRepository;
        this.coursesRepository=coursesRepository;
    }

  public Optional<Teacher> getTeacherById(Long id){
        return teacherRepository.findById(id);
  }

  public List<Teacher> getAllTeachers(){
        return teacherRepository.findAll();
  }
  public Teacher getTeacherByName(String name){
        return teacherRepository.findByName(name);
  }

  public Teacher getTeacherByCourse(Courses course){
        return teacherRepository.findByCourse(course);
  }

    public void saveOrUpdateTeacher(Long teacherId, Teacher teacherData, Long courseId) {
        Teacher teacher;
        if (teacherId != null) {
           teacherRepository.findById(teacherId)
                    .orElseThrow(() -> new RuntimeException("Teacher not found"));
        }
        teacher=teacherData;
        if (courseId != null) {
            Courses course = coursesRepository.findById(courseId)
                    .orElseThrow(() -> new RuntimeException("Course not found"));
            if (course.getTeacher() != null && !course.getTeacher().getId().equals(teacher.getId())) {
                throw new RuntimeException("Course is already assigned to another teacher");
            }
            teacher.setCourse(course);
            course.setTeacher(teacher);
        }
  teacherRepository.save(teacher);
    }


    public void deleteTeacher(Long id) {
        Teacher teacher = teacherRepository.findById(id).orElseThrow(
                () -> new RuntimeException("Teacher not found with ID: " + id)
        );

        // If the teacher has a course, handle the disassociation
        if (teacher.getCourse() != null) {
            Courses course = teacher.getCourse();
            course.setTeacher(null); // Remove the association
            coursesRepository.save(course);
        }

        teacherRepository.deleteById(id);
    }
}
