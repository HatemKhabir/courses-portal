package com.example.coursemanagement.DAO;

import com.example.coursemanagement.entity.Courses;
import com.example.coursemanagement.entity.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TeacherRepository extends JpaRepository<Teacher,Long> {
Teacher findByName(String Name);
Teacher findByCourse(Courses course);
}
