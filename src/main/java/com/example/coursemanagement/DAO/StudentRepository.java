package com.example.coursemanagement.DAO;

import com.example.coursemanagement.entity.Courses;
import com.example.coursemanagement.entity.Majors;
import com.example.coursemanagement.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Set;

public interface StudentRepository extends JpaRepository<Student,Long> {
    List<Student> findByMajor(Majors major);
    List<Student> findByCourses(Courses Course);


}
