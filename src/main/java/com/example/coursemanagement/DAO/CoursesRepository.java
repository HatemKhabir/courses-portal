package com.example.coursemanagement.DAO;

import com.example.coursemanagement.entity.Courses;
import com.example.coursemanagement.entity.Languages;
import com.example.coursemanagement.entity.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CoursesRepository extends JpaRepository<Courses,Long> {
    List<Courses> findByLanguage(Languages lang);
    Courses findByTeacher(Teacher teacher);
}
